import 'package:flutter/material.dart';
import 'package:responsiveness_demo/widgets/app_drawer/app_drawer.dart';

import '../../main.dart';

// display the mobile UI in portrait mode
class HomeMobilePortrait extends StatelessWidget {
  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    // return MyHomePage();
    return Scaffold(
      key: _scaffoldKey,
      drawer: AppDrawer(),
      body: MyHomePage(),
    );
  }
}

// display the mobile UI in portrait mode
class HomeMobileLandscape extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyHomePage();
  }
}
