import 'package:flutter/material.dart';
import 'package:responsiveness_demo/responsive/orientation_layout.dart';
import 'package:responsiveness_demo/responsive/screen_type_layout.dart';
import 'package:responsiveness_demo/views/home/home_view_desktop.dart';
import 'package:responsiveness_demo/views/home/home_view_mobile.dart';
import 'package:responsiveness_demo/views/home/home_view_tablet.dart';

class HomeView extends StatelessWidget {
  HomeView({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: OrientationLayout(
        portrait: HomeMobilePortrait(),
        landscape: HomeMobileLandscape(),
      ),
      tablet: HomeViewTablet(),
      desktop: HomeViewDesktop(),
    );
  }
}
