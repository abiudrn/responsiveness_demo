// Orientation is easily fetched through the MediaQuery but using that directly
// will force conditionals in every view that needs to build using it.

// This will provide us with two properties where we can provide the portrait and landscape widgets that we'd like to show.

import 'package:flutter/cupertino.dart';

class OrientationLayout extends StatelessWidget {
  final Widget landscape;
  final Widget portrait;
  const OrientationLayout({
    Key key,
    this.landscape,
    this.portrait,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var orientation = MediaQuery.of(context).orientation;

    if (orientation == Orientation.landscape) {
      return landscape ?? portrait;
    }

    return portrait;
  }
}
