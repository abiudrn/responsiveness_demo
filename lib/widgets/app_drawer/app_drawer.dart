import 'package:flutter/material.dart';
import 'package:responsiveness_demo/responsive/orientation_layout.dart';
import 'package:responsiveness_demo/responsive/screen_type_layout.dart';
import 'package:responsiveness_demo/widgets/app_drawer/app_drawer_mobile.dart';
import 'package:responsiveness_demo/widgets/app_drawer/app_drawer_tablet.dart';
import 'package:responsiveness_demo/widgets/drawer_option/drawer_option.dart';

class AppDrawer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return ScreenTypeLayout(
      mobile: AppDrawerMobile(),
      tablet: OrientationLayout(
        portrait: AppDrawerTabletPortrait(),
        landscape: AppDrawerTabletLandScape(),
      ),
      desktop: AppDrawerTabletPortrait(),
    );
  }

// a list of the options for the drawer
  static List<Widget> getDrawerOptions() {
    return [
      DrawerOption(title: 'Foods', iconData: Icons.local_dining),
      DrawerOption(title: 'Beverages', iconData: Icons.local_drink),
      DrawerOption(title: 'Favorites', iconData: Icons.favorite_border),
      DrawerOption(title: 'Settings', iconData: Icons.settings)
    ];
  }
}
