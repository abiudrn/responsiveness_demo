import 'package:device_info/device_info.dart';
import 'package:flutter/material.dart';
import 'package:local_auth/local_auth.dart';

// checking device capabilities
// for the purpose of this demo we are going to check if the device has the following capabilities:
// 1. Camera access
// 2. Fingerprint access
// 3. Touch ID or faceID access
// 4. microphone access

class DeviceInformationPage extends StatefulWidget {
  @override
  _DeviceInformationPageState createState() => _DeviceInformationPageState();
}

class _DeviceInformationPageState extends State<DeviceInformationPage> {
  final LocalAuthentication _localAuthentication = LocalAuthentication();
  final DeviceInfoPlugin deviceInfo = DeviceInfoPlugin();
  String deviceModel = 'null';
  bool _hasFPSupport = false;
  String connection = 'NONE';
  // list of avalable biometric authentication supports of your device will be saved in this array
  List<BiometricType> _availableBiometricType = List<BiometricType>();

  // this method checks whether your device has biometric support or not
  Future<void> getBiometricSupport() async {
    bool hasFPSupport = await _localAuthentication.canCheckBiometrics;

    if (!mounted) return;
    setState(() {
      _hasFPSupport = hasFPSupport;
    });
  }

  Future<void> _getAvailableSupport() async {
    // gets all the available biometric supports of the device
    List<BiometricType> availableBiometricType = List<BiometricType>();
    try {
      availableBiometricType =
          await _localAuthentication.getAvailableBiometrics();
    } catch (e) {
      print(e);
    }
    if (!mounted) return;
    setState(() {
      _availableBiometricType = availableBiometricType;
    });
  }

  Future<AndroidDeviceInfo> _getDeviceInfo() async {
    var _deviceModel = 'null';

    //android information
    AndroidDeviceInfo androidInfo = await deviceInfo.androidInfo;
    _deviceModel = androidInfo.model;
    deviceModel = _deviceModel.toString();
  }

  // connectivity
  // var connectivityResult = await (Connectivity().checkConnectivity());
  // if (connectivityResult == ConnectivityResult.mobile) {
  //   connection = 'MOBILE';
  // } else if (connectivityResult == ConnectivityResult.wifi) {
  //   connection = 'WIFI';
  // }

  @override
  void initState() {
    _getAvailableSupport();
    _getAvailableSupport();
    _getDeviceInfo();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: Color(0xFF21BFBD),
        body: ListView(
          children: <Widget>[
            Padding(
              padding: EdgeInsets.only(top: 15.0, left: 10.0),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  IconButton(
                    icon: Icon(Icons.arrow_back_ios),
                    color: Colors.white,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
                  // IconButton(
                  //   icon: Icon(Icons.menu),
                  //   color: Colors.white,
                  //   onPressed: () {},
                  // ),
                  IconButton(
                    icon: Icon(Icons.filter_list),
                    color: Colors.white,
                    onPressed: () {},
                  ),
                ],
              ),
            ),
            SizedBox(
              height: 25.0,
            ),
            Padding(
              padding: EdgeInsets.only(left: 40.0),
              child: Row(
                children: <Widget>[
                  Text(
                    'Device Capabilities',
                    style: TextStyle(
                        fontFamily: 'Montserrat',
                        color: Colors.white,
                        fontSize: 25.0),
                  ),
                ],
              ),
            ),
            SizedBox(height: 40.0),
            Container(
              child: Column(
                children: <Widget>[
                  Text('Supports FP? => $_hasFPSupport'),
                  SizedBox(height: 10.0),
                  Text(
                      'Available Bio types: ${_availableBiometricType.toString()}'),
                  SizedBox(height: 10.0),
                  Text('Device model: $deviceModel'),
                  SizedBox(height: 10.0),
                  Text('Connection: $connection')
                ],
              ),
            )
          ],
        ));
  }
}
